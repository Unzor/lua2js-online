# Lua2js Online
A wrapper over Lua.js, a Lua to JavaScript transpiler.

Try it out [here.](https://example.com)

# License
Lua.js is released under the Apache license. For more information, check out the project's Github page.
